#include <stdlib.h>
#include <GL/glut.h>
#include <fstream>
#include <iostream>
#include <algorithm> 
#include <stdio.h>
#include <sstream>
#include <string>
#include <vector>
#include "glm/vec3.hpp"
#include <glm/vec3.hpp>
#include <glm/vec4.hpp> 
#include <glm/mat4x4.hpp> 
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <iostream>

#define INDEX(x, y) ((x) + (y) * N)

#define PI 3.14
#define VELOCITY_MULTIPLIER 100
#define VELOCITY_CONSTRAINT 1000
#define SCALE 4
#define DENSITY_AMOUNT 40
#define CANDLE_VELOCITY 1000
#define N 128
#define ITERATIONS 16
#define MIN_H 1
#define MIN_W 1
#define FLAME_SIZE 10
#define CANDLE_WIDTH 50
#define CANDLE_HEIGHT 100
#define WICK_WIDTH 1
#define WICK_HEIGHT 25
#define ANGLE_PERCENTAGE 60
#define VELOCITY_AMOUNT 2
#define REFRESH_MILLIS 10
#define FADE_FACTOR 1.02

int mouseXlast = 0;
int mouseYlast = 0;

enum Color {
	WHITE, RED, GREEN, BLUE
};

Color color = WHITE;

enum Mode {
	CANDLE, FREEFORM
};

Mode mode = FREEFORM;

glm::vec3 wick_color = glm::vec3(0.5f, 0.5f, 0.5f);
glm::vec3 flame_color = glm::vec3(0.5f, 0.01f, 0.01f);
glm::vec3 candle_color = glm::vec3(0.5f, 0.35f, 0.05f);

GLuint window;
GLuint position_x = 100, position_y = 100;

void reshape(int width, int height);
void draw();
void handle_key_press(unsigned char theKey, int mouseX, int mouseY);
void timer(int value);
void handle_mouse_move(int x, int y);
void handle_mouse_click(int button, int state, int x, int y);
void move_fluid(int x, int y);

int actual_x(int x);
int actual_y(int y);

void draw_candle();
void draw_candle(int x, int y);
void draw_wick(int x, int y);
void draw_flame(int x, int y);
void draw_candle_body(int x, int y);

float random_angle();

void switch_mode();
bool check_index(int index);
void drawRect(int x, int y, int w, int h);
float clip(float x, float lower, float upper);

class Fluid {
public:
	int size;
	float dt;
	float diff;
	float visc;

	float *s;
	float *density;

	float *Vx;
	float *Vy;

	float *Vx0;
	float *Vy0;

	Fluid(int diffusion, int viscosity, float dt) {
		int arraySize = N * N;

		this->size = N;
		this->dt = dt;
		this->diff = diffusion;
		this->visc = viscosity;

		this->s = (float *)calloc(arraySize, sizeof(float));
		this->density = (float *)calloc(arraySize, sizeof(float));

		this->Vx = (float *)calloc(arraySize, sizeof(float));
		this->Vy = (float *)calloc(arraySize, sizeof(float));

		this->Vx0 = (float *)calloc(arraySize, sizeof(float));
		this->Vy0 = (float *)calloc(arraySize, sizeof(float));
	}

	~Fluid() {
		free(this->s);
		free(this->density);

		free(this->Vx);
		free(this->Vy);

		free(this->Vx0);
		free(this->Vy0);
	}

	void add_density(int x, int y, float amount) {
		int index = INDEX(x, y);
		if (!check_index(index)) {
			return;
		}
		this->density[index] += amount;
	}

	void add_velocity(int x, int y, float amountX, float amountY) {
		int index = INDEX(x, y);
		if (!check_index(index)) {
			return;
		}
		this->Vx[index] += amountX;
		this->Vy[index] += amountY;
	}

	void step() {
		float visc = this->visc;
		float diff = this->diff;
		float dt = this->dt;
		float *Vx = this->Vx;
		float *Vy = this->Vy;
		float *Vx0 = this->Vx0;
		float *Vy0 = this->Vy0;
		float *s = this->s;
		float *density = this->density;

		diffuse_and_bound(Vx0, Vx, visc, dt);
		diffuse_and_bound(Vy0, Vy, visc, dt);

		project(Vx0, Vy0, Vx, Vy);

		advect_horizontal_and_bound(Vx, Vx0, Vx0, Vy0, dt);
		advect_vertical_and_bound(Vy, Vy0, Vx0, Vy0, dt);

		project(Vx, Vy, Vx0, Vy0);

		diffuse_and_bound(s, density, diff, dt);
		advect_and_bound(density, s, Vx, Vy, dt);
	}

	void render(Color color) {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				int x = i * SCALE;
				int y = j * SCALE;
				float d = this->density[INDEX(i, j)];

				switch (color) {
				case BLUE:
					glColor3f(0, 0, d);
					break;
				case GREEN:
					glColor3f(0, d, 0);
					break;
				case RED:
					glColor3f(d, 0, 0);
					break;
				case WHITE:
				default:
					glColor3f(d, d, d);
				}
				drawRect(x, y, SCALE, SCALE);
			}
		}
	}

	void fade() {
		for (int i = 0; i < N * N; i++) {
			this->density[i] /= FADE_FACTOR;
		}
	}

	void set_bounds_old(int b, float* x) {
		for (int i = 1; i < N - 1; i++) {
			x[INDEX(i, 0)] = b == 2 ? -x[INDEX(i, 1)] : x[INDEX(i, 1)];
			x[INDEX(i, N - 1)] = b == 2 ? -x[INDEX(i, N - 2)] : x[INDEX(i, N - 2)];
		}

		for (int j = 1; j < N - 1; j++) {
			x[INDEX(0, j)] = b == 1 ? -x[INDEX(1, j)] : x[INDEX(1, j)];
			x[INDEX(N - 1, j)] = b == 1 ? -x[INDEX(N - 2, j)] : x[INDEX(N - 2, j)];
		}

		set_corners(x);
	}

	void set_bounds(float* x) {
		for (int i = 1; i < N - 1; i++) {
			x[INDEX(i, 0)] = x[INDEX(i, 1)];
			x[INDEX(i, N - 1)] = x[INDEX(i, N - 2)];
		}

		for (int j = 1; j < N - 1; j++) {
			x[INDEX(0, j)] = x[INDEX(1, j)];
			x[INDEX(N - 1, j)] = x[INDEX(N - 2, j)];
		}

		set_corners(x);
	}

	void set_horizontal_bounds(float* x) {
		for (int i = 1; i < N - 1; i++) {
			x[INDEX(i, 0)] = x[INDEX(i, 1)];
			x[INDEX(i, N - 1)] = x[INDEX(i, N - 2)];
		}

		for (int j = 1; j < N - 1; j++) {
			x[INDEX(0, j)] = -x[INDEX(1, j)];
			x[INDEX(N - 1, j)] = -x[INDEX(N - 2, j)];
		}

		set_corners(x);
	}

	void set_vertical_bounds(float* x) {
		for (int i = 1; i < N - 1; i++) {
			x[INDEX(i, 0)] = -x[INDEX(i, 1)];
			x[INDEX(i, N - 1)] = -x[INDEX(i, N - 2)];
		}

		for (int j = 1; j < N - 1; j++) {
			x[INDEX(0, j)] = x[INDEX(1, j)];
			x[INDEX(N - 1, j)] = x[INDEX(N - 2, j)];
		}

		set_corners(x);
	}

	void set_corners(float* x) {
		int top_left = INDEX(0, 0);
		int top_right = INDEX(0, N - 1);
		int bottom_left = INDEX(N - 1, 0);
		int bottom_right = INDEX(N - 1, N - 1);

		x[top_left] = 0.5f * (x[INDEX(1, 0)] + x[INDEX(0, 1)]);
		x[top_right] = 0.5f * (x[INDEX(1, N - 1)] + x[INDEX(0, N - 2)]);
		x[bottom_left] = 0.5f * (x[INDEX(N - 2, 0)] + x[INDEX(N - 1, 1)]);
		x[bottom_right] = 0.5f * (x[INDEX(N - 2, N - 1)] + x[INDEX(N - 1, N - 2)]);
	}

	void diffuse_and_bound(float* x, float* x0, float diff, float dt) {
		diffuse(x, x0, diff, dt);
		set_bounds(x);
	}

	void diffuse(float* x, float* x0, float diff, float dt) {
		float a = dt * diff * (N - 2) * (N - 2);
		linear_solve(x, x0, a, 1 + 6 * a);
	}

	void linear_solve(float* x, float* x0, float a, float c) {
		for (int k = 0; k < ITERATIONS; k++) {
			for (int j = 1; j < N - 1; j++) {
				for (int i = 1; i < N - 1; i++) {
					int current = INDEX(i, j);

					int right = INDEX(i + 1, j);
					int left = INDEX(i - 1, j);
					int top = INDEX(i, j - 1);
					int bottom = INDEX(i, j + 1);

					x[current] = x0[current] + a * (x[right] + x[left] + x[bottom] + x[top]);
					x[current] /= c;
				}
			}
		}
	}

	void project_and_bound(float* velocX, float* velocY, float* p, float* div) {
		project(velocX, velocY, p, div);
		set_horizontal_bounds(velocX);
		set_vertical_bounds(velocY);
	}

	void project(float* velocX, float* velocY, float* p, float* div) {
		for (int j = 1; j < N - 1; j++) {
			for (int i = 1; i < N - 1; i++) {
				int current = INDEX(i, j);

				int right = INDEX(i + 1, j);
				int left = INDEX(i - 1, j);
				int top = INDEX(i, j - 1);
				int bottom = INDEX(i, j + 1);

				div[current] = -0.5f * (velocX[right] - velocX[left] + velocY[bottom] - velocY[top]) / N;
				p[current] = 0;
			}
		}

		set_bounds(div);
		set_bounds(p);
		linear_solve(p, div, 1, 6);
		set_bounds(div);
		set_bounds(p);

		for (int j = 1; j < N - 1; j++) {
			for (int i = 1; i < N - 1; i++) {
				int current = INDEX(i, j);

				int right = INDEX(i + 1, j);
				int left = INDEX(i - 1, j);
				int top = INDEX(i, j - 1);
				int bottom = INDEX(i, j + 1);

				velocX[current] -= 0.5f * (p[right] - p[left]) * N;
				velocY[current] -= 0.5f * (p[bottom] - p[top]) * N;
			}
		}
	}

	void advect_horizontal_and_bound(float* d, float* d0, float* velocX, float* velocY, float dt) {
		advect(d, d0, velocX, velocY, dt);
		set_horizontal_bounds(d);
	}

	void advect_vertical_and_bound(float* d, float* d0, float* velocX, float* velocY, float dt) {
		advect(d, d0, velocX, velocY, dt);
		set_vertical_bounds(d);
	}

	void advect_and_bound(float* d, float* d0, float* velocX, float* velocY, float dt) {
		advect(d, d0, velocX, velocY, dt);
		set_bounds(d);
	}

	void advect(float* d, float* d0, float* velocX, float* velocY, float dt) {
		float dtx = dt * (N - 2);
		float dty = dt * (N - 2);

		for (int j = 1; j < N - 1; j++) {
			for (int i = 1; i < N - 1; i++) {
				float x = i - dtx * velocX[INDEX(i, j)];
				float y = j - dty * velocY[INDEX(i, j)];

				x = clip(x, 0.5f, N + 0.5f);

				float i0 = floorf(x);
				float i1 = i0 + 1;

				y = clip(y, 0.5f, N + 0.5f);

				float j0 = floorf(y);
				float j1 = j0 + 1;

				float s1 = x - i0;
				float s0 = 1 - s1;
				float t1 = y - j0;
				float t0 = 1 - t1;

				int index1 = INDEX(i0, j0);
				int index2 = INDEX(i0, j1);
				int index3 = INDEX(i1, j0);
				int index4 = INDEX(i1, j1);

				if (!(check_index(index1) && check_index(index2) && check_index(index3) && check_index(index4))) {
					return;
				}

				d[INDEX(i, j)] = s0 * (t0 * d0[index1] + t1 * d0[index2]) + s1 * (t0 * d0[index3] + t1 * d0[index4]);
			}
		}
	}
};

Fluid fluid = Fluid(0.2, 0, 0.0001);

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_RGBA | GLUT_DOUBLE);

	glutInitWindowSize(N * SCALE, N * SCALE);
	glutInitWindowPosition(position_x, position_y);

	window = glutCreateWindow("OpenGL Fluid simulation");

	glutDisplayFunc(draw);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(handle_key_press);
	glutTimerFunc(0, timer, 0);
	glutMotionFunc(handle_mouse_move);
	glutMouseFunc(handle_mouse_click);

	glutMainLoop();
	return 0;
}

void reshape(int w, int h)
{
	h = std::max(MIN_H, h);
	w = std::max(MIN_W, w);

	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, w, 0, h);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void draw()
{
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	fluid.step();
	fluid.render(color);
	fluid.fade();

	if (mode == CANDLE) {
		draw_candle();
	}

	glutSwapBuffers();
}

void draw_candle() {
	int x_actual = actual_x(mouseXlast);
	int y_actual = actual_y(mouseYlast);
	float density = rand() % DENSITY_AMOUNT / 2;

	fluid.add_density(x_actual, y_actual, density);

	for (int i = 0; i < VELOCITY_AMOUNT; i++) {
		float angle = random_angle();

		float candle_vel_horizontal = CANDLE_VELOCITY * cos(angle);
		float candle_vel_vertical = CANDLE_VELOCITY * sin(angle);

		fluid.add_velocity(x_actual, y_actual, candle_vel_horizontal, candle_vel_vertical);
	}

	int candle_x = mouseXlast;
	int candle_y = N * SCALE - mouseYlast;

	draw_candle(candle_x, candle_y);
}

float random_angle() {
	return (((rand() % ANGLE_PERCENTAGE + ANGLE_PERCENTAGE / 3) / 100.0f)) * PI;
}

void draw_candle(int x, int y) {
	draw_wick(x, y);
	draw_flame(x, y);
	draw_candle_body(x, y);
}

void draw_wick(int x, int y) {
	glColor3f(wick_color[0], wick_color[1], wick_color[2]);
	drawRect(
		x - WICK_WIDTH / 2,
		y - WICK_HEIGHT,
		WICK_WIDTH,
		WICK_HEIGHT
	);
}

void draw_flame(int x, int y) {
	glColor3f(flame_color[0], flame_color[1], flame_color[2]);
	drawRect(
		x - FLAME_SIZE / 2,
		y - FLAME_SIZE / 2,
		FLAME_SIZE,
		FLAME_SIZE
	);
}

void draw_candle_body(int x, int y) {
	glColor3f(candle_color[0], candle_color[1], candle_color[2]);
	drawRect(
		x - CANDLE_WIDTH / 2,
		y - CANDLE_HEIGHT,
		CANDLE_WIDTH - WICK_WIDTH,
		CANDLE_HEIGHT - WICK_HEIGHT
	);
}

void handle_key_press(unsigned char theKey, int mouseX, int mouseY)
{
	switch (theKey) {
	case 'm':
		switch_mode();
		break;
	case 'b':
		color = BLUE;
		break;
	case 'r':
		color = RED;
		break;
	case 'g':
		color = GREEN;
		break;
	case 'w':
		color = WHITE;
		break;
	}
}

void timer(int value) {
	glutPostRedisplay();
	glutTimerFunc(REFRESH_MILLIS, timer, 0);
}

void handle_mouse_move(int x, int y)
{
	if (mode == FREEFORM) {
		move_fluid(x, y);
	}

	mouseXlast = x;
	mouseYlast = y;
	glutPostRedisplay();
}

void move_fluid(int x, int y) {
	int x_diff = x - mouseXlast;
	int y_diff = mouseYlast - y;

	int x_vel = std::min(VELOCITY_MULTIPLIER * x_diff, VELOCITY_CONSTRAINT);
	int y_vel = std::min(VELOCITY_MULTIPLIER * y_diff, VELOCITY_CONSTRAINT);

	float amountX = std::max(x_vel, -VELOCITY_CONSTRAINT);
	float amountY = std::max(y_vel, -VELOCITY_CONSTRAINT);

	int x_actual = actual_x(x);
	int y_actual = actual_y(y);

	fluid.add_density(x_actual, y_actual, DENSITY_AMOUNT);
	fluid.add_velocity(x_actual, y_actual, amountX, amountY);
}

void handle_mouse_click(int button, int state, int x, int y)
{
	mouseXlast = x;
	mouseYlast = y;
}

int actual_x(int x) {
	return x / SCALE;
}

int actual_y(int y) {
	return  N - y / SCALE;
}

void switch_mode() {
	switch (mode) {
	case CANDLE:
		mode = FREEFORM;
		break;
	case FREEFORM:
		mode = CANDLE;
		break;
	}
}

bool check_index(int index) {
	return index >= 0 && index < N* N;
}

void drawRect(int x, int y, int w, int h) {
	glBegin(GL_QUADS);
	glVertex2f(x, y);
	glVertex2f(x + w, y);
	glVertex2f(x + w, y + h);
	glVertex2f(x, y + h);
	glEnd();
}

float clip(float x, float lower, float upper) {
	return std::min(std::max(lower, x), upper);
}
